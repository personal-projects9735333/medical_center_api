import { SequelizeFindQueryRequest } from "../../utils/models/SequelizeFindQueryRequest";
import UserModel, { UserInput, UserOutput } from "./user.model";

class User{
    public static async findOne(options: any) : Promise<UserOutput> {
        options = options || {};
        const findOptions = {} as SequelizeFindQueryRequest;

        if(options.query){
            findOptions.where = options.query;
        }

        if(options.include){
            findOptions.include = options.include;
        }

        if(options.attributes){
            findOptions.attributes = options.attributes;
        }

        return (await UserModel.findOne(findOptions)) as UserOutput;
    };

    public static async findAll(options?: any) : Promise<UserOutput[]> {
        options = options || {};
        const findOptions = {} as SequelizeFindQueryRequest;

        if(options.query){
            findOptions.where = options.query;
        }

        if(options.include){
            findOptions.include = options.include;
        }

        if(options.attributes){
            findOptions.attributes = options.attributes;
        }

        if(options.limit){
            findOptions.limit = options.limit;
        }

        if(options.offset){
            findOptions.offset = options.offset;
        }

        if(options.order){
            findOptions.order = options.order;
        }

        return (await UserModel.findAll(findOptions)) as UserOutput[];
    };

    public static async create(data: UserInput){
        return await UserModel.create(data);
    }

    public static async update(options: any, data: Partial<UserInput>){
        options = options || {query: {}};
        return await UserModel.update(data, {
            where: options.query,
            returning: true
        });
    }

    public static async delete(options: any){
        options = options || {query :{}};
        return await UserModel.destroy({
            where: options.query
        });
    }
};

export default User;