import { DataTypes, Model, Optional } from "sequelize";
import db from "../../../database/db_config";
import bcrypt from 'bcrypt';

interface UserAttributes {
    id: number;
    name: string;
    phone: string;
    email: string;
    password: string;
    token: string | null;
};

const options = {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    freezeTimeName: true,
};

export interface UserInput extends Optional<UserAttributes, 'id'> {};
export interface UserOutput extends Required<UserAttributes> {};

interface UserInstance extends Model<UserAttributes, UserInput>, UserAttributes {
    id: number;
    name: string;
    phone: string;
    email: string;
    password: string;
    token: string | null;
};

const UserModel = db.define<UserInstance>('user',
    {
        id: {
            primaryKey: true,
            autoIncrement: true,
            type: DataTypes.INTEGER,
            allowNull: false
        },
        name: {
            allowNull: false,
            type: DataTypes.STRING
        },
        phone: {
            allowNull: false,
            type: DataTypes.STRING
        },
        email: {
            allowNull: false,
            type: DataTypes.STRING
        },
        password: {
            allowNull: false,
            type: DataTypes.STRING
        },
        token: {
            allowNull: true,
            type: DataTypes.STRING
        },
    },
    options
);

UserModel.beforeCreate(async (user, _options) => {
    user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10));
});

export default UserModel;