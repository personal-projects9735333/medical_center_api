import { SequelizeFindQueryRequest } from "../../utils/models/SequelizeFindQueryRequest";
import PatientModel, { PatientInput, PatientOutput } from "./patient.model";
class Patient {
    public static async findOne(options: any): Promise<PatientOutput> {
        const findOptions  = {} as SequelizeFindQueryRequest;
        options = options || {};

        if(options.query){
            findOptions.where = options.query;
        }

        if(options.include){
            findOptions.include = options.include;
        }

        if(options.attributes){
            findOptions.attributes = options.attributes;
        }

        return (await PatientModel.findOne(findOptions)) as PatientOutput;
    }

    public static async findAll(options?: any): Promise<PatientOutput[]> {
        const findOptions  = {} as SequelizeFindQueryRequest;
        options = options || {};

        if(options.query){
            findOptions.where = options.query;
        }

        if(options.include){
            findOptions.include = options.include;
        }

        if(options.attributes){
            findOptions.attributes = options.attributes;
        }

        if(options.order){
            findOptions.order = options.order;
        }

        if(options.limit){
            findOptions.limit = options.limit;
        }

        if(options.offset){
            findOptions.offset = options.offset;
        }

        return (await PatientModel.findAll(findOptions)) as PatientOutput[];
    }

    public static async create(data: PatientInput){
        return await PatientModel.create(data);
    }

    public static async update(options: any, data: Partial<PatientInput>){
        options = options || {};
        return await PatientModel.update(data, {
            where: options.query,
            returning: true
        });
    }

    public static async delete(options: any){
        options = options || {};
        return await PatientModel.destroy({
            where: options.query
        });
    }
}

export default Patient;