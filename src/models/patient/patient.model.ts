import { DataTypes, Model, Optional, Sequelize } from "sequelize";
import db from "../../../database/db_config";

interface PatientAttributes {
    id: number;
    name: string;
    age: number;
    phone: string;
    symptoms: string;
    diagnostic: string;
};

export interface PatientInput extends Optional<PatientAttributes, 'id'> { };
export interface PatientOutput extends Required<PatientAttributes> { };

const options = {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    freezeTimeName: true,
};

interface PatientInstance extends Model<PatientAttributes, PatientInput>, PatientAttributes {
    id: number;
    name: string;
    phone: string;
    age: number;
    symptoms: string;
    diagnostic: string;
};

const PatientModel = db.define<PatientInstance>('patient',
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        age: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: false
        },
        symptoms: {
            type: DataTypes.STRING,
            allowNull: false
        },
        diagnostic: {
            type: DataTypes.STRING,
            allowNull: true
        },
    },
    options
);

export default PatientModel;