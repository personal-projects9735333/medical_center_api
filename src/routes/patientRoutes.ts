import { Router } from "express";
import PatientController from "../controllers/patient.controller";
import { createPatientValidator, updatePatientValidator } from '../middlewares/validators/patient.validator'
import { numericIdParamValidator } from "../middlewares/validators/numericIdParam.validator";
const routes = Router();

routes.get('/', PatientController.getAllPatients);
routes.get('/:id', numericIdParamValidator, PatientController.getPatientById);
routes.post('/', createPatientValidator, PatientController.createPatient);
routes.put('/:id', updatePatientValidator, numericIdParamValidator, PatientController.updatePatient);
routes.delete('/:id', numericIdParamValidator, PatientController.deletePatient);

export default routes;