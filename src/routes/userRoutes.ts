import { Router } from "express";
import UserController from "../controllers/user.controller";
import { userCreateValidator, userUpdateValidator } from "../middlewares/validators/user.validator";
import { numericIdParamValidator } from "../middlewares/validators/numericIdParam.validator";
import AuthValidator from "../middlewares/authValidator";

const routes = Router();

routes.get('/login', UserController.login);
routes.get('/', AuthValidator, UserController.getAllUsers);
routes.get('/:id', AuthValidator, UserController.getUserById);
routes.post('/',AuthValidator, userCreateValidator, UserController.createUser);
routes.put('/:id', numericIdParamValidator, userUpdateValidator, UserController.updateUser);
routes.delete('/:id', numericIdParamValidator, UserController.deleteUser);

export default routes;