import { Router } from "express";
import userRouter from '../routes/userRoutes';
import patientRouter from '../routes/patientRoutes';

const router = Router();

router.use('/user', userRouter);
router.use('/patient', patientRouter);

export default router;