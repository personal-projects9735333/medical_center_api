import { Request, Response, NextFunction } from "express";
import User from "../models/user/index";
import { NotFound, Unauthorized } from '../utils/Errors';
import bcrypt from 'bcrypt';
import AuthService from "../services/authService";

class UserController {
    public static async getUserById(req: Request, res: Response, _next: NextFunction) {
        try {
            const userId = req.params.id;
            const user = await User.findOne({
                query: {
                    id: userId
                }
            });

            if (!user) {
                throw new NotFound('User not Found!');
            }

            return res.status(200).json({
                success: true,
                data: user
            });
        } catch (e: any) {
            return res.status(e.statusCode || 500).json({
                success: false,
                message: e.message || e
            });
        }
    }

    public static async getAllUsers(req: Request, res: Response, _next: NextFunction) {
        try {
            const users = await User.findAll();

            return res.status(200).json({
                success: true,
                data: users
            });
        } catch (e: any) {
            return res.status(e.statusCode || 500).json({
                success: false,
                message: e.message || e
            });
        }
    }

    public static async createUser(req: Request, res: Response, _next: NextFunction) {
        try {
            const user = await User.create({
                name: req.body.name,
                email: req.body.email,
                phone: req.body.phone,
                password: req.body.password,
                token: null
            });

            return res.status(200).json({
                success: true,
                data: user
            });
        } catch (e: any) {
            return res.status(e.statusCode || 500).json({
                success: false,
                message: e.message || e
            });
        };
    };

    public static async updateUser(req: Request, res: Response, _next: NextFunction) {
        try {
            const userId = req.params.id;
            const user = await User.findOne({
                query: {
                    id: userId
                }
            });

            if (!user) {
                throw new NotFound('User not Found!');
            }

            await User.update({
                query: {
                    id: userId
                }
            },
                {
                    name: req.body.name,
                    email: req.body.email,
                    phone: req.body.phone,
                    password: req.body.password,
                    token: null
                });

            const updated_user = await User.findOne({ query: { id: userId } });

            return res.status(200).json({
                success: true,
                data: updated_user
            });
        } catch (e: any) {
            return res.status(e.statusCode || 500).json({
                success: false,
                message: e.message || e
            });
        }
    };

    public static async deleteUser(req: Request, res: Response, _next: NextFunction) {
        try {
            const userId = req.params.id;
            const user = await User.findOne({
                query: {
                    id: userId
                }
            });

            if (!user) {
                throw new NotFound('User not Found!');
            }

            await User.delete({ query: { id: userId } });

            return res.status(200).json({
                success: true,
                message: 'Deletion was successful!'
            });
        } catch (e: any) {
            return res.status(e.statusCode || 500).json({
                success: false,
                message: e.message || e
            });
        }
    };

    public static async login(req: Request, res: Response, _next: NextFunction) {
        try {
            const { email, password } = req.body;

            const user = await User.findOne({
                query: {
                    email: email
                }
            });

            if (!user) {
                throw new Unauthorized('Wrong email!');
            }

            if (!bcrypt.compareSync(password, user.password)) {
                throw new Unauthorized('Wrong password!');
            }

            const accessToken = await AuthService.authenticateUser({
                id: user.id,
                email: user.email
            });

            user.password = '';

            return res.status(200).json({
                success: true,
                user,
                accessToken
            });
        } catch (e: any) {
            return res.status(e.statusCode ?? 500).json({
                success: false,
                message: e.message ?? e
            });
        }
    };
}

export default UserController;