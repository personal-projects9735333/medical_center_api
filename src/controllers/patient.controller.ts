import { Request, Response, NextFunction } from "express";
import { NotFound } from '../utils/Errors';
import Patient from "../models/patient";

class PatientController {
    public static async getPatientById(req: Request, res: Response, _next: NextFunction) {
        try {
            const patientId = req.params.id;
            const patient = await Patient.findOne({
                query: {
                    id: patientId
                }
            });

            if (!patient) {
                throw new NotFound('Patient not Found!');
            }

            return res.status(200).json({
                success: true,
                data: patient
            });
        } catch (e: any) {
            return res.status(e.statusCode || 500).json({
                success: false,
                message: e.message || e
            });
        }
    }

    public static async getAllPatients(req: Request, res: Response, _next: NextFunction) {
        try {
            const patients = await Patient.findAll();

            return res.status(200).json({
                success: true,
                data: patients
            });
        } catch (e: any) {
            return res.status(e.statusCode || 500).json({
                success: false,
                message: e.message || e
            });
        }
    }

    public static async createPatient(req: Request, res: Response, _next: NextFunction) {
        try {
            const patient = await Patient.create({
                name: req.body.name,
                age: req.body.age,
                phone: req.body.phone,
                symptoms: req.body.symptoms,
                diagnostic: req.body.diagnostic
            });

            return res.status(200).json({
                success: true,
                data: patient
            });
        } catch (e: any) {
            return res.status(e.statusCode || 500).json({
                success: false,
                message: e.message || e
            });
        };
    };

    public static async updatePatient(req: Request, res: Response, _next: NextFunction) {
        try {
            const patientId = req.params.id;
            const patient = await Patient.findOne({
                query: {
                    id: patientId
                }
            });

            if (!patient) {
                throw new NotFound('Patient not Found!');
            }

            await Patient.update({
                query: {
                    id: patientId
                }
            },
                {
                    name: req.body.name,
                    age: req.body.age,
                    phone: req.body.phone,
                    symptoms: req.body.symptoms,
                    diagnostic: req.body.diagnostic
                });

            const updated_patient = await Patient.findOne({ query: { id: patientId } });

            return res.status(200).json({
                success: true,
                data: updated_patient
            });
        } catch (e: any) {
            return res.status(e.statusCode || 500).json({
                success: false,
                message: e.message || e
            });
        }
    }

    public static async deletePatient(req: Request, res: Response, _next: NextFunction) {
        try {
            const userId = req.params.id;
            const user = await Patient.findOne({
                query: {
                    id: userId
                }
            });

            if (!user) {
                throw new NotFound('Patient not Found!');
            }

            await Patient.delete({ query: { id: userId } });

            return res.status(200).json({
                success: true,
                message: 'Deletion was successful!'
            });
        } catch (e: any) {
            return res.status(e.statusCode || 500).json({
                success: false,
                message: e.message || e
            });
        }
    }
}

export default PatientController;