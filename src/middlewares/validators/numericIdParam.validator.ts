import { NextFunction, Request, Response } from "express";
import { param, validationResult } from "express-validator";

const numericIdParamValidator = [
    param('id').
        notEmpty().withMessage('Id parameter is required!').
        isNumeric().withMessage('Id parameter should be a number!'),
    
    (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            const errMessage = errors.array()[0].msg ? errors.array()[0].msg : 'Validation failed!';
            return res.status(422).json({
                success: false,
                message: errMessage
            });
        }
        next();
    }
]

export { numericIdParamValidator };