import { NextFunction, Request, Response } from "express";
import { body, validationResult } from "express-validator";

const phoneRegex = /^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/;

const createPatientValidator = [
    body('name').
        notEmpty().withMessage('Name is required!').
        isString().withMessage('Name should be a string'),
    body('age').
        notEmpty().withMessage('Age is required!').
        isNumeric().withMessage('Age should be a number!'),
    body('phone').
        notEmpty().withMessage('Phone is required!').
        matches(phoneRegex).withMessage('Invalid Romanian phone number!'),
    body('symptoms').
        notEmpty().withMessage('Symptoms are required!').
        isString().withMessage('Symtoms should be a astring!'),

    (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const errMessage = errors.array()[0].msg ? errors.array()[0].msg : 'Validation failed!';
            return res.status(422).json({
                success: false,
                message: errMessage
            });
        }
        next();
    }
];

const updatePatientValidator = [
    body('name').
        optional().
        notEmpty().withMessage('Name is required!').
        isString().withMessage('Name should be a string'),
    body('age').
        optional().
        notEmpty().withMessage('Age is required!').
        isNumeric().withMessage('Age should be a number!'),
    body('phone').
        optional().
        notEmpty().withMessage('Phone is required!').
        matches(phoneRegex).withMessage('Invalid Romanian phone number!'),
    body('symptoms').
        optional().
        notEmpty().withMessage('Symptoms are required!').
        isString().withMessage('Symtoms should be a astring!'),

    (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const errMessage = errors.array()[0].msg ? errors.array()[0].msg : 'Validation failed!';
            return res.status(422).json({
                success: false,
                message: errMessage
            });
        }
        next();
    }
];

export { createPatientValidator, updatePatientValidator };