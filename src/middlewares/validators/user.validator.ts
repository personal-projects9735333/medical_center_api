import { NextFunction, Request, Response } from 'express';
import { body, validationResult } from 'express-validator';

const phoneRegex = /^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/;

const userCreateValidator = [
    body('name').
        notEmpty().withMessage('User name is required!').
        isString().withMessage('User name should be a string!'),
    body('email').
        notEmpty().withMessage('User email is required!').
        isString().withMessage('User email should be a string!').
        isEmail().withMessage('Invalid email format!'),
        body('phone')
        .notEmpty().withMessage('Phone is required!')
        .matches(phoneRegex).withMessage('Invalid Romanian phone number!'),
    body('password').
        notEmpty().withMessage('User password is required!').
        isString().withMessage('User password should be a string!'),

    (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            const errMessage = errors.array()[0].msg ? errors.array()[0].msg : 'Validation failed!';
            return res.status(422).json({
                success: false,
                message: errMessage
            })
        }
        next();
    }
]

const userUpdateValidator = [
    body('name').
        optional().
        notEmpty().withMessage('User name is required!').
        isString().withMessage('User name should be a string!'),
    body('email').
        optional().
        notEmpty().withMessage('User email is required!').
        isString().withMessage('User email should be a string!').
        isEmail().withMessage('Invalid email format!'),
    body('phone').
        optional().
        notEmpty().withMessage('Phone is required!').
        matches(phoneRegex).withMessage('Invalid Romanian phone number!'),
    body('password').
        not().exists().withMessage('Password is not allowed!'),

    (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            const errMessage = errors.array()[0].msg ? errors.array()[0].msg : 'Validation failed!';
            return res.status(422).json({
                success: false,
                message: errMessage
            })
        }
        next();
    }
]

export { userCreateValidator, userUpdateValidator };