import { NextFunction, Request, Response } from "express"
import { Unauthorized } from "../utils/Errors";
import jwt from 'jsonwebtoken';
import env from '../utils/environment/environment';

const AuthValidator = (req: Request, res: Response, next: NextFunction) => {
    try {
        const token = req.headers.authorization;

        if (!token) {
            throw new Unauthorized('Token Not Found!');
        };

        if (!token.includes(' ')) {
            throw new Unauthorized('Token Not Valid!');
        };

        const splitted_token = token.split(' ')[1];
        const user = jwt.verify(splitted_token, env.SECRET_KEY as string) as any;
        req.body.user_info = {
            id: user.id,
            email: user.email
        };
    } catch (e: any) {
        return res.status(e.statusCode ?? 500).json({
            success: false,
            message: e.message ?? e
        });
    }

    return next();
};

export default AuthValidator;