import express from 'express';
import router from './routes/routes';
import db from '../database/db_config';
import { Sequelize } from 'sequelize';
import env from './utils/environment/environment'
import bodyParser from 'body-parser';

const app = express();
const PORT = env.PORT;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api', router);

const { Umzug, SequelizeStorage } = require('umzug');


const umzug = new Umzug({
    migrations: {
        glob: 'database/migrations/*.js',
        resolve: ({ name, path, context }: { name: string, path: string, context: any }) => {
            const migration = require(path);
            return {
                name,
                up: async () => migration.up(context, Sequelize),
                down: async () => migration.down(context, Sequelize),
            }
        },
    },
    context: db.getQueryInterface(),
    storage: new SequelizeStorage({ sequelize: db }),
    logger: console,
});

(async () => {
    await umzug.up();
    app.listen(PORT, () => {
        console.log(`App is running on port ${PORT}`);
    });
})();