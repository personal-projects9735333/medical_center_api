import jwt from 'jsonwebtoken';
import env from '../utils/environment/environment';


class AuthService {
    public static async authenticateUser(payload: any){
        const accessToken = jwt.sign(payload, env.SECRET_KEY as string, {
            expiresIn: env.EXPIRATION_TIME
        });

        return accessToken;
    };
};

export default AuthService;