require('dotenv').config();

const variables = {
    PORT: process.env.PORT,
    DATABASE_HOST: process.env.DATABASE_HOST,
    DATABASE_DIALECT: process.env.DATABASE_DIALECT,
    DATABASE_USER: process.env.DATABASE_USER,
    DATABASE_PASSWORD: process.env.DATABASE_PASSWORD,
    DATABASE_NAME: process.env.DATABASE_NAME,
    EXPIRATION_TIME: process.env.EXPIRATION_TIME,
    SECRET_KEY: process.env.SECRET_KEY
};

export default variables;