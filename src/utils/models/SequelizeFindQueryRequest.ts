export type SequelizeFindQueryRequest = {
    where: any;
    include: string[];
    attributes: string[];
    limit: number;
    offset: number;
    order: string[];
}