import { Dialect, Sequelize } from "sequelize";
import env from '../src/utils/environment/environment'

const db = new Sequelize(env.DATABASE_NAME as string, env.DATABASE_USER as string, env.DATABASE_PASSWORD, {
    host: env.DATABASE_HOST,
    dialect: env.DATABASE_DIALECT as Dialect
});

export default db;